import React, { Component } from 'react';
import Chart from '../containers/Chart';

class App extends Component {
  static propTypes = {};
  static defaultProps = {};

  render() {
    return (
      <Chart/>
    );
  }
}

export default App;
