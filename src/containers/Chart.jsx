import React, { Component } from 'react';
import { CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis } from 'recharts';


const data = [
  { name: '2018-11-19 09:27:47', 29: 17791, 39: 9327 },
  { name: '2018-11-19 09:28:47', 29: 17795, 39: 9328 },
  { name: '2018-11-19 09:29:47', 29: 17799, 39: 9328 },
  { name: '2018-11-19 09:33:56', 29: 17810, 39: 9329 },
  { name: '2018-11-19 09:34:56', 29: 17813, 39: 9329 },
  { name: '2018-11-19 09:35:56', 29: 17815, 39: 9329 },
  { name: '2018-11-19 09:36:56', 29: 17818, 39: 9329 },
];

class Chart extends Component {
  static propTypes = {};
  static defaultProps = {};

  render() {
    return (
      <div>
        <LineChart height={300} data={data} width={1200}
                   margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
          <XAxis dataKey="name" />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey={39} stroke="#8884d8" name='BANANAFOX' />
          <Line type="monotone" dataKey={29} stroke="#82ca9d" name='ЯПлакалъ' />
        </LineChart>
      </div>
    );
  }
}

export default Chart;
